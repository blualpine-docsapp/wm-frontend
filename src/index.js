import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Route,HashRouter, Switch} from 'react-router-dom';
import InfoTabs from './container/infoTabs/infoTabs';
import HealthInfo from './container/infoTabs/health-info'
import PersonalInfo from './container/infoTabs/personal-info';
import ResultPage from './container/resultPage/resultPage';
ReactDOM.render(
  <HashRouter>
          <Switch>
          <Route exact path='/' component={App}/>
          <Route exact path='/infoTabs' component={InfoTabs}/>
          <Route exact path='/personalInfo' component={PersonalInfo}/>
          <Route exact path='/healthInfo' component={HealthInfo}/>
          <Route exact path='/resultPage' component={ResultPage} />

          </Switch>
  </HashRouter>
, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
