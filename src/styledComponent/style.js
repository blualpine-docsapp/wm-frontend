import styled from "styled-components";
import { device } from './devices';

const Container = styled.div`
  text-align: center;
  justify-content: center;
`;

const Input = styled.input`
  padding: 0.5em;

  margin: 0.5em;

  color: ${props => props.inputColor || "#1caf41"};

  background: rgba(255, 255, 255);

  border: none;

  border-radius: 3px;
`;
const InputStyle = styled.input`
  ${"" /* background: rgba(0, 0, 0, 0.003); */}

  box-shadow: inset 0 -2px 1px rgba(0, 0, 0, 0.03);

  font-size: 15px;
  border: 1px solid #e7e9f1;
  ::placeholder,
  ::-webkit-input-placeholder {
    font-size: 18px;

    color: rgba(232, 235, 239);
  }

  :-ms-input-placeholder {
    font-size: 18px;

    color: rgba(232, 235, 239);
  }
`;

const GreyButton = styled.button`
  background: #555555;

  border-radius: 5em;

  border: 0;

  color: white;

  opacity: 0.9;

  margin: 0 1em;
  margin-top: 4%;

  font-weight: bold;

  padding: 1em 2em;

  font-size: 15px;

  width: 150px;

  height: 50px;

  font-size: 12px;
`;

const GreenButton = styled.button`
  background: #1caf41;

  border-radius: 5em;

  border: 0;

  color: white;

  opacity: 0.9;

  margin: 0 1em;

  font-weight: bold;
  margin-top: 4%;

  padding: 1em 2em;

  font-size: 15px;

  width: 150px;

  height: 50px;

  font-size: 12px;
`;

const PlanButton = styled.button`
  background: #1caf41;

  border-radius: 5em;

  border: 0;

  color: white;

  opacity: 0.9;

  margin: 0 1em;

  font-weight: bold;
  margin-bottom: 4%;
  position: fixed;
  padding: 1em 2em;
  bottom: 0;
  font-size: 15px;

  width: 330px;

  height: 40px;

  font-size: 12px;
`;

const Button = styled.button`
  background: #1caf41;
  border-radius: 5em;
  border: 0;
  color: white;
  opacity: 0.9;
  margin: 0 1em;
  font-weight: 600;
  padding: 1em 2em;
  margin-top: 4%;
  width: 206px;
  height: 50px;
  
`;

export { Button, GreenButton, Container, InputStyle, Input, GreyButton , PlanButton};
