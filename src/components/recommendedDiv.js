import React from "react";
import Loading from "../components/loadingBar";
import { IP } from "../config";
import Images from "../weightManagement";
import "whatwg-fetch";

var details = [];
export default class RecommendedPlan extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }

  componentDidMount() {
    this.setState({ skipped: localStorage.getItem("skipped") });
    this.getResultData();
  }

  getResultData() {
    details = [];
    fetch(IP + "weightManagement/details?apikey=123", {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => response.json())
      .then(responseData => {
        if (responseData != null) {
          this.setState({
            packageDetailsData: responseData.packageDetails,
            priceCardData: responseData.priceCard,
            doctorsData: responseData.doctors,
            testimonialsData: responseData.testimonials,
            loading: false
          });
        }
      });
  }

  render() {
    
    // eslint-disable-next-line react/no-direct-mutation-state
    this.state.skipped = localStorage.getItem("skipped");

    if (this.state.loading) {
      return (
        <div className="tabsLoading">
          <Loading />
        </div>
      );
    }
    return (
      <div>
        <div
          className={
            this.state.skipped
              ? "recommendedPlanDivSkipped"
              : "recommendedPlanDiv"
          }
        >
          <p>Recommended Plan</p>
          <div
            className={this.state.skipped ? "weightPlanSkipped" : "weightPlan"}
          >
            <div className="weightPlan">
            <h3>{this.state.priceCardData.packageName}</h3>
            </div>
          </div>
        </div>
        {this.state.packageDetailsData.features.map(item => (
          <div>
            {item.position % 2 != 0 ? (
              <div className="boxGridright">
                <div className="leftText">
                  <p>{item.title}</p>
                  <p className="smallText">{item.description}</p>
                </div>
                <div className="rightText">
                  {item.imageURL == null ? (
                    <img src={Images.cardImage} alt="" />
                  ) : (
                    <img src={item.imageURL} alt="" />
                  )}
                </div>
              </div>
            ) : (
              <div className="boxGridleft">
                <div className="leftText">
                {item.imageURL == null ? (
                    <img src={Images.cardImage} alt="" />
                  ) : (
                    <img src={item.imageURL} alt="" />
                  )}
                </div>
                <div className="rightText">
                  <p>{item.title}</p>
                  <p className="smallText">{item.description}</p>
                </div>
              </div>
            )}
          </div>
        ))}
        <div className="topTrainers">
          <p>Top Dieticians and Trainers</p>

          <div className="trainerCardBox">
            {this.state.doctorsData.map(item => (
              <div className="trainerCard">
                {item.profileURL == null ||
                  item.profileURL == undefined ||
                  item.profileURL == "" ? (
                  <img src={Images.docImage} alt="" />
                ) : (
                  <img src={item.profileURL} alt="" />
                )}
                <p className="name">{item.name}</p>
                <p>{item.experience} years</p>
              </div>
            ))}
          </div>
        </div>
        <div className="testimonials">
          <p>400 + Users have benefitted from the Plan</p>
          <div className="cardInline">
            {this.state.testimonialsData.map(item => (
              <div className="cardView">
                <p className="testimonialsHeading">
                  {item.name}, {item.age}
                </p>
                {/* {item.profileURL == null || 
                  item.profileURL == undefined ||
                  item.profileURL == "" ? (
                  <img
                    src={Images.benefittedImage}
                    className="userImages"
                    alt="userImage"
                  />
                ) : (
                  <img
                    src={item.profileURL}
                    className="userImages"
                    alt="userImage" 
                  />
                )}
                <div className="statusButton">
                  <div className="label">
                    <p>Before</p>
                  </div>
                </div> */}
                <p>{item.heading}</p>
                <p className="smallLabel">{item.description}</p>
                <div className="iconInline">
                  <img src={Images.location} className="mapIcon" alt="" />
                  <p className="smallLabel">{item.location}</p>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}
