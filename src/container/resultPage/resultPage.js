import React from "react";
import Loading from "../../components/loadingBar";
import Recommended from "../../components/recommendedDiv";
import ReactSpeedometer from "react-d3-speedometer";
import Images from "../../weightManagement";
import { Link } from "react-router-dom";
import { Line } from "react-chartjs-2";
import { PlanButton } from "../../styledComponent/style";
import "./resultPage.css";
import "whatwg-fetch";
import captureEvents from '../../captureEvent'
var res;
var parsedRes = {};
let className = "";
var queryString=""
var patientId
var eventType
var topic
let data, plugins, legendOpts, options;

export default class ResultPage extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      width: window.innerWidth
    };
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentDidMount = () => {
    localStorage.getItem("queryString");
    localStorage.getItem("queryParam");
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions);
    // eslint-disable-next-line react/no-direct-mutation-state
    if (!this.state.skipped) {
      // eslint-disable-next-line no-restricted-globals
      history.pushState(null, null, location.href);
      window.onpopstate = function (event) {
        // eslint-disable-next-line no-restricted-globals
        history.go(1);
      };
    }
    res = localStorage.getItem("res");
    parsedRes = { ...JSON.parse(res) };
    setTimeout(() => {
      // eslint-disable-next-line react/no-direct-mutation-state
      this.setState({ loading: false });
    }, 3000);

    if (window.performance) {
      if (performance.navigation.type === 1) {
        window.location.replace("/?queryString");
      }
    }
  };

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions);
    localStorage.removeItem("nameText");
    localStorage.removeItem("age");
    localStorage.removeItem("weight");
    localStorage.removeItem("height");
    localStorage.removeItem("gender");
    localStorage.removeItem("res");
    localStorage.removeItem("selected");
    localStorage.removeItem("historyOf");
  }

  updateDimensions = () => {
    this.setState({ width: window.innerWidth });
  };

  doGraphCustomization = () => {
    let min, max;

    if (parsedRes.graphDetails.currentWeight > parsedRes.graphDetails.goal) {
      min = parsedRes.graphDetails.goal - 0.5;
      max = parsedRes.graphDetails.currentWeight + 0.5;
    } else {
      min = parsedRes.graphDetails.currentWeight - 0.5;
      max = parsedRes.graphDetails.goal + 0.5;
    }

    options = {
      showAllTooltips: false,
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        xAxes: [
          {
            ticks: {
              display: true,
              fontSize: 9
            },
            gridLines: {
              display: true,
              // drawBorder: true,
              tickMarkLength: 40,
              offsetGridLines: false,
              drawTicks: false,
              drawOnChartArea: false
            }
          }
        ],
        yAxes: [
          {
            ticks: { display: false, min: min, max: max },
            gridLines: {
              display: false,
              drawBorder: false
            }
          }
        ]
      }
    };

    legendOpts = {
      display: false,
      label: {
        boxWidth: 10
      }
    };

    if (parsedRes.graphDetails.currentWeight > parsedRes.graphDetails.goal) {
      plugins = [
        {
          afterDraw: (chartInstance, easing) => {
            const ctx = chartInstance.chart.ctx;
            ctx.fillStyle = "#04bc0f";
            ctx.font = "bold 10px Arial";

            const width = window.screen.width;
            let newWidth = width - 98;
            let w = (newWidth / width) * 100;
            // console.log("WIDTH %%%%::: ", w); 
            const wi = (w * window.screen.width) / 100;
            // console.log("wi = ", wi);
            const djwid = window.screen.width - (window.screen.width * 0.63);
            // console.log("total window.screen.width = ", window.screen.width);

            // console.log("djwid = ", djwid);
            // console.log("Final WIDTH:: ", wi);
            // ctx.fillText(parsedRes.graphDetails.goal + " Kg", djwid, 105);
            switch (window.screen.width) {
              case 360:
                ctx.fillText(parsedRes.graphDetails.goal + " Kg", 305, 105);
                break;

                case 411:
                  ctx.fillText(parsedRes.graphDetails.goal + " Kg", 322, 105);
                  break;
              case 412:
                ctx.fillText(parsedRes.graphDetails.goal + " Kg", 315, 105);
                break;
              case 320:
                ctx.fillText(parsedRes.graphDetails.goal + " Kg", 278, 105);
                break;

              case 375:
                ctx.fillText(parsedRes.graphDetails.goal + " Kg", 290, 105);
                break;

              case 414:
                ctx.fillText(parsedRes.graphDetails.goal + " Kg", 310, 105);
                break;

              case 404:
                ctx.fillText(parsedRes.graphDetails.goal + " Kg", 300, 105);
                break;

              case 391:
                ctx.fillText(parsedRes.graphDetails.goal + " Kg", 297, 105);
                break;

              case 368:
                ctx.fillText(parsedRes.graphDetails.goal + " Kg", 310, 105);
                break;
              case 393:
                  ctx.fillText(parsedRes.graphDetails.goal + " Kg", 300, 105);
                break;

              default:
                ctx.fillText(parsedRes.graphDetails.goal + " Kg", 280, 105);
            }
            ctx.fillStyle = "rgba(143, 27, 27, 0.6)";
            ctx.fillText(parsedRes.graphDetails.currentWeight + " Kg", 56, 9);
            // ctx.beginPath();
            // ctx.lineWidth = 0.2;
            // ctx.setLineDash([5, 1.5]);
            // ctx.strokeStyle = "#979797";
            // ctx.moveTo(67, 120);
            // ctx.lineTo(67, 23);
            // ctx.stroke();
            // ctx.beginPath();
            // ctx.lineWidth = 0.2;
            // ctx.setLineDash([5, 1.5]);
            // ctx.strokeStyle = "#979797";
            // // ctx.moveTo(x, topY);
            // ctx.moveTo(300, 23);
            // // ctx.lineTo(x, bottomY);
            // ctx.lineTo(300, 23);
            // ctx.stroke();
          }
        }
      ];
    } else if (
      parsedRes.graphDetails.currentWeight < parsedRes.graphDetails.goal
    ) {
      plugins = [
        {
          afterDraw: (chartInstance, easing) => {
            const ctx = chartInstance.chart.ctx;
            ctx.fillStyle = "#04bc0f";
            ctx.font = "bold 10px Arial";
            switch (window.screen.width) {
              case 360:
                ctx.fillText(parsedRes.graphDetails.goal + " Kg", 305, 9);
                break;

              case 411:
                ctx.fillText(parsedRes.graphDetails.goal + " Kg", 315, 9);
                break;

              case 412:
                  ctx.fillText(parsedRes.graphDetails.goal + " Kg", 315, 9);
                  break;

                case 393:
                    ctx.fillText(parsedRes.graphDetails.goal + " Kg", 300, 9);
                  break;

              case 320:
                ctx.fillText(parsedRes.graphDetails.goal + " Kg", 278, 9);
                break;

              case 375:
                ctx.fillText(parsedRes.graphDetails.goal + " Kg", 290, 9);
                break;

              case 414:
                ctx.fillText(parsedRes.graphDetails.goal + " Kg", 310, 9);
                break;

              case 404:
                ctx.fillText(parsedRes.graphDetails.goal + " Kg", 300, 9);
                break;

              case 391:
                ctx.fillText(parsedRes.graphDetails.goal + " Kg", 297, 9);
                break;

              case 368:
                ctx.fillText(parsedRes.graphDetails.goal + " Kg", 310, 9);
                break;

              default:
                ctx.fillText(parsedRes.graphDetails.goal + " Kg", 280, 9);
            }
            ctx.fillStyle = "rgba(143, 27, 27, 0.6)";
            ctx.fillText(parsedRes.graphDetails.currentWeight + " Kg", 56, 105);
            // ctx.beginPath();
            // ctx.lineWidth = 0.2;
            // ctx.setLineDash([5, 1.5]);
            // ctx.strokeStyle = "#979797";
            // ctx.moveTo(67, 120);
            // ctx.lineTo(67, 23);
            // ctx.stroke();
            // ctx.beginPath();
            // ctx.lineWidth = 0.2;
            // ctx.setLineDash([5, 1.5]);
            // ctx.strokeStyle = "#979797";
            // // ctx.moveTo(x, topY);

            // ctx.moveTo(300, 23);
            // // ctx.lineTo(x, bottomY);
            // ctx.lineTo(300, 23);
            // ctx.stroke();
          }
        }
      ];
    }
  };

  makeGraph = () => {
    data = canvas => {
      const ctx = canvas.getContext("2d");

      const gradient = ctx.createLinearGradient(0, 0, 0, 140);
      gradient.addColorStop(0, "#bba2f5");
      gradient.addColorStop(0.3, "#d2c0fc");
      gradient.addColorStop(0.6, "#e8deff");
      gradient.addColorStop(0.7, "#f0ebfc");
      gradient.addColorStop(0.9, "rgba(255,255,255)");

      const gradientStroke = ctx.createLinearGradient(
        0,
        0,
        window.screen.availWidth,
        0
      );
      gradientStroke.addColorStop(0, "#5a38aa");
      gradientStroke.addColorStop(0.2, "#10a873");
      gradientStroke.addColorStop(0.4, "#04bc0f");

      let diff =
        parsedRes.graphDetails.currentWeight - parsedRes.graphDetails.goal;
      diff = diff.toString().replace("-", "");

      diff = parseInt(diff);
      let division = diff / 4;
      let div1, div2, div3;

      let noOfWeeks = parseInt(parsedRes.graphDetails.duration / 7, 10);
      let labels = [];
      labels.push("");
      labels.push("Current");
      for (let i = 1; i <= noOfWeeks; i++) {
        labels.push("Week " + i);
      }

      labels.push("");

      if (parsedRes.graphDetails.currentWeight > parsedRes.graphDetails.goal) {
        div1 = parsedRes.graphDetails.currentWeight - division;
        div2 = div1 - division;
        div3 = div2 - division;
      } else {
        div1 = parsedRes.graphDetails.currentWeight + division;
        div2 = div1 + division;
        div3 = div2 + division;
      }

      let pointColors = [];
      let pointRadius = [];
      let pointBGColors = [];
      let pointBorderWidth = [];

      for (let i = 0; i < labels.length; i++) {
        if (i === 0 || i + 1 === labels.length) {
          pointColors.push("rgba(90,56,170,1)");
          pointBGColors.push("rgba(90,56,170,1)");
          pointRadius.push(0);
          pointBorderWidth.push(0);
        } else if (i + 1 === labels.length - 1) {
          pointColors.push("rgba(166, 237, 184, 0.9)");
          pointBGColors.push("#04bc0f");
          pointRadius.push(3.3);
          // pointBorderWidth.push(25.0)
          pointBorderWidth.push(0.0);
        } else {
          pointColors.push("rgba(90,56,170,1)");
          pointBGColors.push("rgba(90,56,170,1)");
          pointRadius.push(1.6);
          pointBorderWidth.push(0);
        }
      }

      return {
        labels: labels,
        datasets: [
          {
            type: "line",
            label: "",
            backgroundColor: gradient,
            borderColor: gradientStroke,
            borderWidth: 1.2,
            pointRadius: pointRadius,
            pointBorderWidth: pointBorderWidth,
            pointBorderColor: pointColors,
            pointBackgroundColor: pointBGColors,
            data: [
              parseInt(parsedRes.graphDetails.currentWeight - 0.5, 10),
              parseInt(parsedRes.graphDetails.currentWeight, 10),
              div1,
              div2,
              div3,
              parsedRes.graphDetails.goal,
              parsedRes.graphDetails.goal - 0.5
            ]
          }
        ]
      };
    };
  };

  startPayment() {
    patientId = localStorage.getItem("queryString")
    eventType = "wm_get_plan_page"
    if(patientId!=="undefined"){
      captureEvents(patientId,eventType)
      }
    if (window.android) {
      window.android.startPayment();
    }
  }
  takeTest() {
  
    patientId = localStorage.getItem("queryString")
    eventType = "wm_take_test"
    if(patientId!=="undefined"){
      captureEvents(patientId,eventType)
      }
  }
  takeTestResultPage(){
    patientId = localStorage.getItem("queryString")
    eventType = "wm_take_test_result_page"
    if(patientId!=="undefined"){
      captureEvents(patientId,eventType)
      }    
    }
  getCallFromDietacian(){
    patientId = localStorage.getItem("queryString")
    eventType = "wm_call_click"
    if(patientId!=="undefined"){
      captureEvents(patientId,eventType)
      }  
    }
  getCallFromDietacianResultPage(){
    patientId = localStorage.getItem("queryString")
    eventType = "wm_call_click_result_page"

    if(patientId!=="undefined"){
      captureEvents(patientId,eventType)
      }  
    }
  render() {
    const { width } = this.state;
    // eslint-disable-next-line react/no-direct-mutation-state
    this.state.skipped = localStorage.getItem("skipped");

    if (this.state.loading) {
      if (this.state.skipped) {
        return (
          <div className="loadingDiv">
            <Loading />
          </div>
        );
      } else {
        return (
          <div>
            <p className="Our-Professionals-ar">
              Our Professionals are Preparing <br />
              your report
            </p>
            <div className="loadingDiv">
              <Loading />
            </div>
          </div>
        );
      }
    }

    if (!this.state.skipped) {
      const status = parsedRes.bmi.status;
      const needle = parseFloat(parsedRes.bmi.value);
      var health;
      var value;
      switch (status) {
        case "Normal": {
          className = "BmiMeterNormal";
          value = 22.5 > needle > 18.5 ? 27 : 25 > needle > 22.5 ? 29 : 23;
          health = "Let's work on";
          break;
        }
        case "Obese": {
          className = "BmiMeterObese";
          value = needle < 36 ? 37 : needle > 40 ? 39 : needle;
          health = "Health Risks you are prone to";
          break;
        }
        case "Overweight": {
          className = "BmiMeterOverWeight";
          value = needle < 32 ? 34 : needle > 36 ? 35 : needle;
          health = "Health Risks you are prone to";
          break;
        }
        case "Severely Obese": {
          className = "BmiMeterSeverlyObese";
          value = needle < 40 ? 42 : needle > 44 ? 43 : needle;
          health = "Health Risks you are prone to";
          break;
        }
        case "Severely Underweight": {
          className = "BmiMeterSeverelyUnderWeight";
          value = needle < 10 ? 10 : needle > 16 ? 15 : needle;
          health = "Health Risks you are prone to";
          break;
        }
        case "Underweight": {
          className = "BmiMeterUnderWeight";
          value =
            17.5 >= needle >= 16
              ? needle
              : 18.5 > needle > 17.5
                ? 17.5
                : needle;
          health = "Health Risks you are prone to";
          break;
        }
        case "Very SEVERELY OBESE": {
          className = "BmiMeterVerySeverelyObese";
          value = 43;
          health = "Health Risks you are prone to";
          break;
        }
        case "Very Severely Underweight": {
          className = "BmiMeterSeverelyUnderWeight";
          value = 11;
          health = "Health Risks you are prone to";
          break;
        }
        default: {
        }
      }
      this.makeGraph();
      this.doGraphCustomization();
    }
    return (
      <center className="pageCenter">
        {/* Skip Test Layout */}
        <div>
          {this.state.skipped === "true" ? (
            <div>
            {/* Hide temporarily */}
              {/* <div className="tipsCard">
                <div className="cardOne">
                  <img src={Images.surgery} alt="" className="imageCurve" />
                  <img src={Images.play} className="playIcon" alt="" />
                  <p className="cardName">
                    Reduce your weight with Monthly plans
                  </p>
                </div>
                <div className="cardOne">
                  <img src={Images.surgery} alt="" className="imageCurve" />
                  <img src={Images.play} className="playIcon" alt="" />
                  <p className="cardName">
                    Reduce your weight with Monthly plans
                  </p>
                </div>
              </div> */}
              <div className="weightHealthy">
                <img
                  src={Images.weightHealthy}
                  alt=""
                  className="weightHealthy"
                />
                <div className="planTextLeft">
                  <h3>Is your Weight Healthy ?</h3>
                  <p className="smallText">
                    {" "}
                    Check your Body Mass Index and Health Risk Status.
                  </p>
                  <Link
                    to={{
                      pathname: "/",
                      search: "?queryString"
                    }}
                  >
                    <button type="submit" className="cardButton" onClick={() => this.takeTest()}>
                      Take Test
                    </button>
                  </Link>
                </div>
              </div>
              <div className="planBoxTwo">
                <div className="planTextLeft">
                  <img
                    src={Images.femaleDoctor}
                    alt=""
                    className="femaleDoctor"
                  />
                </div> 
                <div className="planTextRight">
                  <h2>Achieve your weight goals !</h2>
                  <p className="smallText">
                    {" "}
                    Get your diet and fitness plans prepared by our
                    professionals{" "}
                  </p>
                  <a href="tel:+916366384735"  data-rel="external">
                    <button type="submit" className="cardButtonOne" onClick={() => this.getCallFromDietacian()} >
                      Get a call from Dietician{" "}
                    </button>
                  </a>
                </div>
              </div>
            </div>
          ) : (
              <div>
                <div className="callDoctor" style={{ width: "width" }}>
                  <div className="leftSide">
                    {parsedRes.doctorDetails.imageurl === null ||
                      parsedRes.doctorDetails.imageurl === "NULL" ? (
                        <img
                          src={Images.docImage}
                          width="40"
                          height="40"
                          className="docImage"
                          alt=""
                        />
                      ) : (
                        <img
                          src={parsedRes.doctorDetails.imageurl}
                          width="40"
                          height="40"
                          className="docImage"
                          alt=""
                        />
                      )}
                    <p className="docName">{parsedRes.doctorDetails.name}</p>
                    <p className="designation">
                      {parsedRes.doctorDetails.speciality}
                    </p>
                  </div>
                  <div className="rightSide1">
                    <a href="tel:+916366384735" data-rel="external" >
                      <img src={Images.callIcon} alt="" className="callIcon" onClick={() => this.getCallFromDietacianResultPage()} />
                    </a>
                  </div>
                </div>
                <div className={className}>
                  <div className="ResultDiv">
                    <div className="borderWidth"></div>
                    <div className="container">
                      <div className="leftSide">
                        <p className="userName">
                          Hello {parsedRes.patientDetails.name},
                      </p>
                        {/* <p className="small">High Risk</p>
                    <div className="progressBar">
                      <rcLine.Line
                        percent={50}
                        strokeWidth={0.5}
                        strokeColor="#dc6c47"
                      />
                    </div> */}
                      </div>
                      <div className="rightSide">
                        <Link
                          to={{
                            pathname: "/",
                            search: "?queryString"
                          }}
                        >
                          <button className="userTest" onClick={()=>this.takeTestResultPage()}>Test Again  </button>
                        </Link>
                      </div>
                    </div>
                    <p className="userDescription">{parsedRes.bmi.text}</p>
                  </div>
                  <h3 className="healthStatus">
                    {parsedRes.bmi.status.toString().toUpperCase()}
                  </h3>
                  <ReactSpeedometer
                    segments={6}
                    customSegmentStops={[10, 16, 22, 32, 36, 40, 44]}
                    height={249}
                    width={249}
                    minValue={10}
                    maxValue={44}
                    value={value}
                    ringWidth={8}
                    labelFontSize="0"
                    textColor={"#fff"}
                    segmentColors={[
                      "#09346f",
                      "#134d9e",
                      "#10a873",
                      "#e63f00",
                      "#dc1818",
                      "#a30e00"
                    ]}
                    needleColor="white"
                    needleTransitionDuration={3000}
                    needleHeightRatio={0.75}
                  />
                  <div id="sign1"></div>
                  <div id="sign2"></div>
                  <div id="sign3"></div>
                  <div id="sign4"></div>
                  <div id="sign5"></div>
                  <div id="circle"></div>
                  <div id="circleNew"></div>
                  <div id="circle1">
                    <p className="bmiText">Your BMI</p>
                    <h2 className="bmiValue">{parsedRes.bmi.value}</h2>
                    {/* <p className="bmiLimit">0.0 over Normal Limit</p> */}
                  </div>
                </div>
                {parsedRes.healthRisks.length > 0 ? (
                  <div className="healthRisk">
                    <p>{health}</p>
                    <div className="diseaseCardWrapper">
                      {parsedRes.healthRisks.map(item => (
                        <div className="diseaseDiv1">
                          {
                            (item.image !== null && item.image !== undefined && item.image !== "") || (item.imageUrl !== null && item.imageUrl !== undefined && item.imageUrl !== "") ?
                              item.imageUrl == null ||
                                item.imageUrl == undefined ||
                                item.imageUrl == "" ? (
                                  <img src={item.image} alt="" />
                                ) : (
                                  <img src={item.imageUrl} alt="" />
                                )
                              :
                              (
                                <img src={Images.anemia} alt="" className="" />
                              )
                          }
                          <p className="diseaseTitle">{item.name}</p>
                          {/* {item.description == null || item.description == "" || item.description == undefined
                    ?
                    <p className="smallDetail">
                      An uncurable breakdown of cartiledge,normally seen over
                      the age of 35
                    </p>
                    :
                    <p className="smallDetail">
                     {item.description}
                    </p>
                    } */}
                        </div>
                      ))}
                    </div>
                  </div>
                ) : (
                    <div></div>
                  )}
                {typeof parsedRes.graphDetails.goal != "object" ? (
                  <div className="goalDiv">
                    <p>Recommended Goal</p>
                    <div className="goalGraph">
                      <div className="graph">
                        <Line
                          ref={reference => (this.rChart = reference)}
                          data={data}
                          options={options}
                          legend={legendOpts}
                          height={150}
                          plugins={plugins}
                        />
                      </div>
                      <div className="top">
                        <img src={Images.arrow} alt="" className="lossImg" />
                        <p className="shapePara">
                          {parsedRes.graphDetails.currentWeight >
                            parsedRes.graphDetails.goal
                            ? parsedRes.graphDetails.currentWeight -
                            parsedRes.graphDetails.goal
                            : parsedRes.graphDetails.goal -
                            parsedRes.graphDetails.currentWeight}{" "}
                          kg{" "}
                          {parsedRes.graphDetails.currentWeight >
                            parsedRes.graphDetails.goal
                            ? "Loss"
                            : "Gain"}{" "}
                          in a Month
                      </p>
                      </div>
                    </div>
                  </div>
                ) : (
                    <div></div>
                  )}
              </div>
            )}
        </div>
        {/* Skip test layout end */}
        {/* Recommended Plan Component */}
        <Recommended />
        {/* Recommended Plan Component Ends */}
        <center>
          <button
            type="submit"
            className="planButton"
            onClick={() => this.startPayment()}
          >
            Get your First Month Plan{" "}
          </button>
        </center>
      </center>
    );
  }
}
