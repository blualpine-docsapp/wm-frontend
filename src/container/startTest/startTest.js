import React from "react";
import { Button } from "../../styledComponent/style";
import { Link } from "react-router-dom";
import Images from '../../weightManagement';
import "./startTest.css";
import captureEvents from "../../captureEvent"
var queryString;
var queryParam;
var patientId;
var eventType;
var topic;
export default class firstPage extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      skipped: true, 
      queryString: "",
      queryParam: ""
    }
  }

  componentWillUnmount() {
    localStorage.removeItem("nameText");
    localStorage.removeItem("age");
    localStorage.removeItem("weight");
    localStorage.removeItem("feet");
    localStorage.removeItem("inch");
    localStorage.removeItem("height");
    localStorage.removeItem("selected");
    localStorage.removeItem("historyOf");
    localStorage.removeItem("gender");
  }

  skipTest() {
    localStorage.setItem("skipped", this.state.skipped);
    patientId = localStorage.getItem('queryString')
    eventType = "wm_skip_test"
    if(patientId!=="undefined"){
      captureEvents(patientId,eventType)
      }
    }

  startTest() {
    localStorage.removeItem("skipped")
    patientId = localStorage.getItem('queryString')
    eventType = "wm_start_test"
    console.log("TCL: firstPage -> startTest -> topic", topic)

    if(patientId!=="undefined"){
      captureEvents(patientId,eventType)
      }

  }

  componentDidMount() {
    // preventing the page to go back
    // eslint-disable-next-line no-restricted-globals
    history.pushState(null, null, location.href);
    window.onpopstate = function (event) {
      // eslint-disable-next-line no-restricted-globals
      history.go(1);
    };
    queryParam = window.location.href.toString().split('?')[1];
    localStorage.setItem("queryParam", queryParam)
    queryString = window.location.href.toString().split('?')[1].split('=')[1];
    console.log("TCL: firstPage -> componentDidMount -> queryString", queryString)
    localStorage.setItem("queryString", queryString)
  }

  render() {
    return (
      <div className="containerViewImg">
        <div className="mainviewStyle">
          <img src={Images.startImage} alt="group" />
          <p className="textInputStyle">
            You are two steps away from getting your<br></br>
            Personalised Health Risk Report and<br></br>
            Weight Management Plan
          </p>
          <center>
            <Link to="/infoTabs">
              <Button onClick={() => this.startTest()}>Start My Test</Button>
            </Link>
          </center>
          <Link to="/resultPage" className ="skipLinkStyle">
          <p className="skipTest" onClick={() => this.skipTest()}>
              SKIP TEST
            </p>
          </Link>
        </div>
      </div>
    );
  }
}
