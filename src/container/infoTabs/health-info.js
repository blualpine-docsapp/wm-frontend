import React from "react";
import "./infoTabs.css";
import Images from '../../weightManagement';
import captureEvents from '../../captureEvent'
var selectedId;
var h;
let selected = [];
var queryString="";
var patientId
var topic
var eventType
let historyOf = [];
export default class HealthInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      defaultIndex: null,
      demoData: [
        {
          id: 4,
          unselectIcon: Images.smoking2,
          selectedIcon: Images.smoking,
          isChecked: false
        },
        {
          id: 5,
          unselectIcon: Images.drinking2,
          selectedIcon: Images.cupImg,
          isChecked: false
        },
        {
          id: 6,
          unselectIcon: Images.none2,
          selectedIcon: Images.closeImg,
          isChecked: false
        }
      ]
    };
  }

  componentDidMount() {
    this.props.onRef(this)
  }

  componentWillUnmount() {
    selected = [];
    historyOf = [];
  }

  isNull(toCheck, mandatory) {

    if ((toCheck === null || toCheck === undefined || toCheck === "") && localStorage.getItem(mandatory) !== 1) {
      return true;
    }
    return false;
  }

  isEmpty(toCheck) {
    if (toCheck.length === 0) {
      return true;
    } 
    return false;
  }
  indulgeEvent(){
    patientId = localStorage.getItem("queryString")
    eventType = "wm_form_indulge"
    if(patientId!=="undefined"){
      captureEvents(patientId,eventType)
      }  
    }
  familyHistoryEvent(){
    patientId = localStorage.getItem("queryString")
    eventType = "wm_form_fam_history"
    if(patientId!=="undefined"){
      captureEvents(patientId,eventType)
      } 
    }
  handleGetResultButton() {
    if (
      this.isNull(localStorage.getItem("nameText"), "NAME") ||
      this.isNull(localStorage.getItem("age"), "AGE") ||
      this.isNull(localStorage.getItem("weight"), "WEIGHT") ||
      this.isNull(localStorage.getItem("feet"), "HEIGHT") ||
      this.isNull(localStorage.getItem("inch"), "HEIGHT") ||
      (localStorage.getItem("GENDER") !== 1 && localStorage.getItem("gender") === null) ||
      (localStorage.getItem("GENDER") !== 1 && localStorage.getItem("gender").length === 0) ||
      this.isEmpty(selected) ||
      this.isEmpty(historyOf)
    ) {
      // disable next button
      this.props.getResultButtonStatus(true);
    } else {
      //enable
      this.props.getResultButtonStatus(false);
    }
  }

  clickImge(index, id) {

    h = id;
    if (selectedId === id) {
      id = null;
    }
    selectedId = id;

    // this.setState({ defaultIndex: index });

    if (h === 6) {
      // eslint-disable-next-line react/no-direct-mutation-state
      this.state.demoData[0].isChecked = false;
      // eslint-disable-next-line react/no-direct-mutation-state
      this.state.demoData[1].isChecked = false;
      // eslint-disable-next-line react/no-direct-mutation-state
      this.state.demoData[2].isChecked = !this.state.demoData[2].isChecked;

      if (this.state.demoData[2].isChecked) {
        selected.push(6);
      } else {
        selected = [];
      }
      selected = selected.filter(s => {
        return s !== 4 && s !== 5;
      });
    } else {
      // eslint-disable-next-line react/no-direct-mutation-state
      this.state.demoData[2].isChecked = false;
      switch (h) {
        case 4: {
          // eslint-disable-next-line react/no-direct-mutation-state
          this.state.demoData[0].isChecked = !this.state.demoData[0].isChecked;
          selected = selected.filter(s => {
            return s !== 6;
          });

          if (this.state.demoData[0].isChecked) {
            selected.push(4);
          } else {
            selected = selected.filter(s => {
              return s !== 4;
            });
          }

          break;
        }
        case 5: {
          // eslint-disable-next-line react/no-direct-mutation-state
          this.state.demoData[1].isChecked = !this.state.demoData[1].isChecked;

          selected = selected.filter(s => {
            return s !== 6;
          });

          if (this.state.demoData[1].isChecked) {
            selected.push(5);
          } else {
            selected = selected.filter(s => {
              return s !== 5;
            });
          }

          break;
        }
        default: {
          selected = selected.filter(s => {
            return s !== 6;
          });
        }
      }
    }

    localStorage.setItem("selected", selected);
    this.handleGetResultButton();
  }

  checkBoxClicked(item, index) {
    // eslint-disable-next-line react/no-direct-mutation-state
    this.props.maqData[1].options[
      index
    ].isClicked = this.props.maqData[1].options[index].hasOwnProperty(
      "isClicked"
    )
        ? !this.props.maqData[1].options[index].isClicked
        : true;

    if (this.props.maqData[1].options[index].isClicked) {
      historyOf.push(this.props.maqData[1].options[index].id);
    } else {
      historyOf = historyOf.filter(h => {
        return h != this.props.maqData[1].options[index].id;
      });
    }
    localStorage.setItem("historyOf", historyOf);
    this.handleGetResultButton();
  }

  render() {
    return (
      <div>
        <div className="healthInfoCard">
          <div className="styleComponent" />
          <div className="textStyle">
            <p className="indulgeText">{this.props.maqData[0].text}</p>
          </div>
          <div className="rowstyleSmoking">
            {this.state.demoData.map((item, key) => (
              <div className="fullArea">
                <div />
                <div>
                <img
                  src={item.isChecked ? item.selectedIcon : item.unselectIcon}
                  className={item.isChecked ? "indulgeSelectImg" : "indulgeImg"}
                  onClick={() => {
                    this.clickImge(key, item.id) 
                    this.indulgeEvent()                 
                  }}
                  alt=""
                />
                </div>
              </div>
            ))}
          </div>
          <div className="styleComponent" />
        </div>
        <div className="healthInfoHistroyCard">
          <div className="styleComponent" />
          <div className="healthInfoSpacing">
            <div className="textHistoryStyle">
              <p className="historyText">{this.props.maqData[1].text}</p>
            </div>
            <div>
              {this.props.maqData[1].options.map((index, item) => (
                <div className="checkBoxStyle">
                  <label className="container">
                    <input
                      type="checkbox"
                      checked={
                        item.isClicked
                      }
                      onClick={index => {
                        this.checkBoxClicked(index, item)
                        this.familyHistoryEvent()
                      }}
                    />
                    <span className="checkmark"></span>
                  </label>
                  <p className="checkboxTextStyle">{index.text}</p>
                </div>
              ))}
            </div>
          </div>
        </div>
        <div className="styleComponent" />
      </div>
    );
  }
}