import React from "react";
import { InputStyle, Input } from "../../styledComponent/style";
import "./infoTabs.css";
import Images from "../../weightManagement";
import captureEvents from "../../captureEvent"
var gender = [];
var queryString =""
var patientId
var eventType
var topic
export default class PersonalInfo extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      age: "",
      weight: "",
      fit: "",
      inch: "",
      height: "",
      fillBlanks: "",
      loading: true,
      fitText: "",
      nameText: "",
      genderData: [
        {
          id: 0,
          unselectIcon: Images.maleImg,
          selectedIcon: Images.maleColor,
          isChecked: false
        },
        {
          id: 1,
          unselectIcon: Images.femaleImg,
          selectedIcon: Images.femaleColor,
          isChecked: false
        }
      ]
    };
  }

  isNull(toCheck, mandatory) {
    if (
      (toCheck === null || toCheck === undefined || toCheck === "") &&
      localStorage.getItem(mandatory) !== 1
    ) {
      return true;
    }
    return false;
  }

  lengthConvert() {
    if (
      this.state.inch !== undefined &&
      this.state.inch !== null &&
      this.state.inch !== "" &&
      this.state.feet !== undefined &&
      this.state.feet !== null &&
      this.state.feet !== ""
    ) {
      let inchInCm = parseInt(this.state.inch) / 0.3937;
      let feetInCm = parseInt(this.state.feet) / 0.032808;
      let height = feetInCm + inchInCm;

      localStorage.setItem("height", height);
    }
  }
  handleNextButton() {
    if (
      this.isNull(localStorage.getItem("nameText"), "NAME") ||
      this.isNull(localStorage.getItem("age"), "AGE") ||
      localStorage.getItem("age").length !== 2 ||
      this.isNull(localStorage.getItem("weight"), "WEIGHT") ||
      this.isNull(localStorage.getItem("feet"), "HEIGHT") || parseInt(localStorage.getItem("feet")) < 3 || parseInt(localStorage.getItem("feet")) > 10 ||
      this.isNull(localStorage.getItem("inch"), "HEIGHT") ||
      (localStorage.getItem("GENDER") !== 1 &&
        localStorage.getItem("gender") === null) ||
      (localStorage.getItem("GENDER") !== 1 &&
        localStorage.getItem("gender").length === 0)
    ) {
      // disable next button
      this.props.nextButtonStatus(true);
    } else {
      //enable
      this.props.nextButtonStatus(false);
    }
  }
  handleSubmit(event) {
    event.preventDefault();
  }
  nameEvent() {
    patientId = localStorage.getItem("queryString")
    eventType = "wm_form_name"
    if(patientId!=="undefined"){
      captureEvents(patientId,eventType)
      }  
    }
  ageEvent(){
    patientId = localStorage.getItem("queryString")
    eventType = "wm_form_age"
    if(patientId!=="undefined"){
      captureEvents(patientId,eventType)
      }  
    }
  genderEvent(){
    patientId = localStorage.getItem("queryString")
    eventType = "wm_form_gender"
    if(patientId!=="undefined"){
      captureEvents(patientId,eventType)
      }    
  }
  weightEvent(){
    patientId = localStorage.getItem("queryString")
    eventType = "wm_form_weight"
    if(patientId!=="undefined"){
      captureEvents(patientId,eventType)
      }    
  }
  heightEvent(){
    patientId = localStorage.getItem("queryString")
    eventType = "wm_form_height"

    if(patientId!=="undefined"){
      captureEvents(patientId,eventType)
      }  
    }
  handleName = e => {
    if (e.target.value.match("^[a-zA-Z ]*$") != null) {
      this.setState({ nameText: e.target.value });
      localStorage.setItem("nameText", e.target.value);
      this.handleNextButton();
    }
  };
  
  handleAge = e => {
    if (
      e.target.value.toString().length === 1 ||
      e.target.value.toString().length === 2 ||
      e.target.value === ""
    ) {
      if (e.target.value === "") {
        this.setState({ age: e.target.value });
        localStorage.setItem("age", e.target.value);
      } else if (
        e.target.value.toString().length === 1 &&
        e.target.value !== "9"
      ) {
        this.setState({ age: e.target.value });
        localStorage.setItem("age", e.target.value);
      } else if (e.target.value.toString().length === 2) {
        if (e.target.value >= 15 && e.target.value <= 80) {
          this.setState({ age: e.target.value });
          localStorage.setItem("age", e.target.value);
        }
      }
    }
    this.handleNextButton();
  };
  handleWeight = e => {
    if (
      e.target.value.match("\\b(1?[0-9]{1,2}|2[0-4][0-9]|25[0])\\b") != null ||
      e.target.value === ""
    ) {
      this.setState({ weight: e.target.value });
      localStorage.setItem("weight", e.target.value);
      this.handleNextButton();
    }
  };

  handleInch = e => {
    localStorage.setItem("inch", e.target.value);
    // eslint-disable-next-line react/no-direct-mutation-state
    this.state.inch = e.target.value;
    this.setState({});
    this.lengthConvert();
    this.handleNextButton();
  };

  handleFeet = e => {
    this.setState({ feet: "" });
    if (
      e.target.value.toString().length === 1 ||
      e.target.value.toString().length === 2 ||
      e.target.value === ""
    ) {
      if (e.target.value === "") {
        this.setState({ feet: e.target.value });
        localStorage.setItem("feet", e.target.value);
      } else if (
        (e.target.value.toString().length === 1 && e.target.value >= 3) ||
        e.target.value == 1
      ) {
        this.setState({ feet: e.target.value });
        localStorage.setItem("feet", e.target.value);
      } else if (e.target.value.toString().length === 2) {
        if (e.target.value >= 3 && e.target.value <= 10) {
          this.setState({ feet: e.target.value });
          localStorage.setItem("feet", e.target.value);
        }
      }
    }
    localStorage.setItem("feet", e.target.value);
    // eslint-disable-next-line react/no-direct-mutation-state
    this.state.feet = e.target.value;
    this.setState({});
    this.lengthConvert();
    this.handleNextButton();
  };

  genderChecked(index) {
    if (index === 0) {
      // eslint-disable-next-line react/no-direct-mutation-state
      this.state.genderData[0].isChecked = !this.state.genderData[0].isChecked;

      // eslint-disable-next-line react/no-direct-mutation-state
      this.state.genderData[1].isChecked = false;

      if (this.state.genderData[0].isChecked) {
        gender.push(index);
        gender = gender.filter(g => {
          return g !== 1;
        });
      } else {
        gender = gender.filter(g => {
          return g !== 0 && g !== 1;
        });
      }
    }

    if (index === 1) {
      // eslint-disable-next-line react/no-direct-mutation-state
      this.state.genderData[1].isChecked = !this.state.genderData[1].isChecked;

      // eslint-disable-next-line react/no-direct-mutation-state
      this.state.genderData[0].isChecked = false;

      if (this.state.genderData[1].isChecked) {
        gender.push(index);
        gender = gender.filter(g => {
          return g !== 0;
        });
      } else {
        gender = gender.filter(g => {
          return g !== 0 && g !== 1;
        });
      }
    }
    localStorage.setItem("gender", gender);
  }

  focusMethodFit = function getFocus() {
    document.getElementById("fitId").focus();
  };
  focusMethodKg = function getFocus() {
    document.getElementById("kgId").focus();
  };
  render() {
    return (
      <div>
        <div className="yourStyleContainer">
          <div className="yourStyleView">
            <InputStyle
              name="name"
              className="yourNameStyle"
              placeholder={this.props.fillBlanks[0].text}
              type="text"
              value={this.state.nameText}
              onChange={e => this.handleName(e)}
              onFocus={()=>this.nameEvent()}
            />
          </div>
        </div>
        <div className="styleComponent" />
        <div className="yourStyleContainer">
          <div className="maleAndFemaleStyle">
            <div className="styleComponent" />
            <div className="maleFemaleStyle">
              <div className="viewStyleAge">
                <p className="ageTextViewStyle">
                  {this.props.fillBlanks[1].text}{" "}
                </p>
                <div className="ageBoxStyle">
                  <input
                    name="age"
                    onChange={e => this.handleAge(e)}
                    className="ageInput"
                    type="number"
                    maxLength={2}
                    value={this.state.age}
                    onFocus={()=>this.ageEvent()}
                  />
                  <sub className="yearStyle">
                    <strong>yrs</strong>
                  </sub>
                </div>
              </div>
            </div>

            <div className="maleStyle">
              {this.state.genderData.map((item, index) => (
                <div className="genderView">
                  <p className="maleTextViwStyle">
                    {this.props.mcqData[0].options[index].text}
                  </p>
                  <img
                    src={item.isChecked ? item.selectedIcon : item.unselectIcon}
                    className="maleCircle"
                    onClick={() => {
                      this.genderChecked(index);
                      this.handleNextButton();
                      this.setState({});
                      this.genderEvent();
                    }}
                    
                    alt=""
                  />
                </div>
              ))}
            </div>

          </div>
        </div>
        <div className="styleComponent" />
        <div className="flexCenter">
          <div className="heightWeightStyle">
            <div className="heightStyle">
              <div className="styleComponent" />
              <p className="heightText">{this.props.fillBlanks[2].text}</p>
              <div>
                <div className="inMainStyle">
                  <input
                    name="feet"
                    onChange={e => {
                      this.handleFeet(e);
                    }}
                    className="fitStyle"
                    type="number"
                    maxLength={1}
                    id="fitId"
                    value={this.state.feet}
                    onFocus={()=>this.heightEvent()}
                  />
                  <sub className="inTextStyle">ft</sub>
                  <input
                    name="inch"
                    onChange={e => {
                      this.handleInch(e);
                    }}
                    className="fitStyleIn"
                    type="number"
                    maxLength={2}
                    id="inId"
                  />
                  <sub className="inTextStyleIn">in</sub>
                </div>
              </div>
            </div>
            <div className="weightStyle" onClick={this.focusMethodKg}>
              <div className="styleComponent" />
              <p className="heightText">{this.props.fillBlanks[3].text}</p>
              <div className="mainStyle">
                <div className="heightFlex">
                  <input
                    name="weight"
                    onChange={e => this.handleWeight(e)}
                    className="kgStyles"
                    type="number"
                    value={this.state.weight}
                    id="kgId"
                    onFocus={()=>this.weightEvent()}
                  />
                  <sub className="kgTextStyle">Kg</sub>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="styleComponent" />
      </div>
    );
  }
}
