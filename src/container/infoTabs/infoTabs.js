import React from "react";
import "./infoTabs.css";
import {
  GreenButton,
  Container,
  GreyButton
} from "../../styledComponent/style";
import PersonalInfo from "./personal-info";
import HealthInfo from "./health-info";
import back from "../../assets/back.png";
import Slider from "react-slick";
import { createBrowserHistory } from "history";
import { IP } from "../../config";
import Loading from "../../components/loadingBar";
import 'whatwg-fetch';
export const history = createBrowserHistory();
var res;
var fill_blanks = [];
var mcq = [];
var maq = [];
var mandatory = [];
var images;
var data = [];
export default class infoTabs extends React.Component {
  constructor(props) {
    super(props);
    this.nextButtonStatus = this.nextButtonStatus.bind(this);
    this.getResultButtonStatus = this.getResultButtonStatus.bind(this);
    this.state = {
      step1: true,
      slideCount: "0",
      loading: true,
      status: true,
      getResultStatus: true,
      images: ""
    };
  }


  componentDidMount() {
    localStorage.getItem("queryString");
    if (window.performance) {
      if (performance.navigation.type === 1) {
        window.location.replace('/?queryString')
      }
    }
    this.getTestData();
    localStorage.getItem("nameText");
    localStorage.getItem("age");
    localStorage.getItem("gender");
    localStorage.getItem("weight");
    localStorage.getItem("height");
    localStorage.getItem("selected");
    localStorage.getItem("historyOf");
    localStorage.getItem("queryString");
  }
  getTestData() {
    fill_blanks = [];
    mcq = [];
    maq = [];
    fetch(IP + "weightManagement/takeTest?apikey=123", {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => response.json())
      .then(responseData => {
        if (responseData != null) {
          for (let i = 0; i < responseData.questions.length; i++) {
            mandatory.push({
              "label": responseData.questions[i].label,
              "mandatory": responseData.questions[i].mandatory
            });

            localStorage.setItem(responseData.questions[i].label.toUpperCase(), responseData.questions[i].mandatory);

            if (responseData.questions[i].type === "FILL_BLANKS") {
              fill_blanks.push(responseData.questions[i]);
            } else if (responseData.questions[i].type === "MCQ") {
              mcq.push(responseData.questions[i]);
            } else {
              maq.push(responseData.questions[i]);
            }
          }
          maq[1].options = maq[1].options.map(item => {
            item.isClicked = false;
            return item;
          });
          this.setState({
            fillBlanks: fill_blanks,
            mcqData: mcq,
            maqData: maq
          });
          // eslint-disable-next-line react/no-direct-mutation-state
          this.state.loading = false;
          this.setState({});
        }
      });
  }

  getResult() {
    data = [];
    let nameText = localStorage.getItem("nameText");
    let age = localStorage.getItem("age");
    let gender = localStorage.getItem("gender");
    let weight = localStorage.getItem("weight");
    let height = localStorage.getItem("height");
    let indulgeId = localStorage.getItem("selected");
    let historyOf = localStorage.getItem("historyOf");
    let queryString = localStorage.getItem("queryString")
    console.log("TCL: infoTabs -> getResult -> queryString", queryString)
    fetch(IP + "weightManagement/submitTest?apikey=123", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        patientId: queryString,
        answers: [
          {
            questionId: 1,
            label: "name",
            optionId: [],
            text: nameText
          },
          {
            questionId: 2,
            label: "age",
            optionId: [],
            text: age
          },
          {
            questionId: 3,
            label: "gender",
            optionId: [],
            text: gender
          },
          {
            questionId: 4,
            label: "height",
            optionId: [],
            text: height,
            unit: "cms"
          },
          {
            questionId: 5,
            label: "weight",
            optionId: [],
            text: weight
          },
          {
            questionId: 6,
            label: "indulgeIn",
            optionId: indulgeId,
            text: null
          }, 
          {
            questionId: 7,
            label: "historyOf",
            optionId: [],
            text: historyOf
          }
        ]
      })
    })
      .then(response => response.json())
      .then(responseData => {
        res = responseData;
        if (res.hasOwnProperty("success")) {
          if (res.success === 0) {
            alert("Sorry! We did not get result for you");
          }
        } else {
          
          localStorage.setItem("res", JSON.stringify(res));
          data.push({
          images: "../../src/assets"+res.healthRisks.tag+".png"
          })
          localStorage.setItem("images", images)
          this.props.history.push("/resultPage");
          // this.setState({images: "../../src/assets"+res.healthRisks.tag+".png" })
          localStorage.setItem("images", data)
        }
      });
  }

  handleBackArrow = () => {
    this.slider.slickPrev();
    if (this.state.step1) {
      this.props.history.push('/?queryString');
    } else {
      // eslint-disable-next-line react/no-direct-mutation-state
      this.state.step1 = true;
      this.setState({});
    }
  };

  onNext() {
    this.slider.slickNext();
    this.setState(() => ({
      step1: !this.state.step1
    }));
  }

  onDone() {
    this.getResult();
  }

  nextButtonStatus(status) {
    this.setState({ status: status });
  }

  getResultButtonStatus(status) {
    this.setState({ getResultStatus: status });
  }

  render() {
    if (this.state.loading) {
      return (
        <div className="tabsLoading">
       <Loading />
       </div>
      );
    }

    var settings = {
      infinite: false,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      focusOnSelect: false
    };

 
    return (
      <div>
        <div className="containerView">
          <img
            src={back}
            className="backstyle"
            onClick={this.handleBackArrow}
            alt="handleBackArrow"
          />
          <div className="step-indicator-wrapper">
            <button className="step-indicator active" />
            <button
              className={`step-indicator ${
                !this.state.step1 ? "active" : "step-indicator"
                }`}
            />
            <div className="marginText" />
            <p className="Step-1-of-2">
              {this.state.step1 && this.state.slideCount != 1
                ? "Step 1 of 2"
                : "Step 2 of 2"}
            </p>
          </div> 
        </div>

        <Container>
          <Slider
            ref={c => (this.slider = c)}
            // focusOnSelect={false}
            afterChange={index => {
              // this.setState(() => ({
              //   step1: this.state.step1 ? false : true
              // }));

              // this.slider.slickNext();
              this.setState(() => ({
                step1: index !== 0 ? false : true
              }));

              this.setState({ slideCount: index });
              this.child.handleGetResultButton();
            }}
            {...settings}
          >
            <div>
              {" "}
              <PersonalInfo
                nextButtonStatus={this.nextButtonStatus}
            
                fillBlanks={this.state.fillBlanks}
                mandatory={mandatory}
                mcqData={this.state.mcqData}
              />{" "}
            </div>
            <div>
              {" "}
              <HealthInfo
                getResultButtonStatus={this.getResultButtonStatus}
                mandatory={mandatory}
                onRef={ref => (this.child = ref)}
                maqData={this.state.maqData}
              />{" "}
            </div>
          </Slider>

          {/* <div className="styleComponent" /> */}
          <div className="ButtonComponent">
            {
              this.state.step1 ?
                !this.state.status ? (
                  <GreenButton
                    type="submit"
                    onClick={() => {
                      this.state.step1 && this.state.slideCount != 1
                        ? this.onNext()
                        : this.onDone();
                    }}
                  >
                    {this.state.step1 && this.state.slideCount != 1
                      ? "Next"
                      : "Get Result"}
                  </GreenButton>
                ) : (
                    <GreyButton>
                      {this.state.step1 && this.state.slideCount != 1
                        ? "Next"
                        : "Get Result"}
                    </GreyButton>
                  ) : !this.state.getResultStatus && !this.state.status ? (
                    <GreenButton
                      type="submit"
                      onClick={() => {
                        this.state.step1 && this.state.slideCount != 1
                          ? this.onNext()
                          : this.onDone();
                      }}
                    >
                      {this.state.step1 && this.state.slideCount != 1
                        ? "Next"
                        : "Get Result"}
                    </GreenButton>
                  ) : (
                    <GreyButton>
                      {this.state.step1 && this.state.slideCount != 1
                        ? "Next"
                        : "Get Result"}
                    </GreyButton>
                  )
            }
          </div>
        </Container>
      </div>
    );
  }
}