import React from 'react';
import './App.css';
import FirstPage from './container/startTest/startTest';
import captureEvents from './captureEvent'
function App() {
  return (
    <div className="App">  
         <FirstPage/>
    </div>
  );
}
var patientId = localStorage.getItem("queryString")
var eventType = "wm_home_click"
if(patientId!=="undefined"){
  captureEvents(patientId,eventType)
  }
        
export default App;
