function captureEvents(patientId,eventType){
    fetch("https://androidmobileapi.docsapp.in/patient/event", {
    method: "POST",
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
    },
        body: JSON.stringify({
        event: eventType,
        patientId: patientId,
        packageId:"6001",
        id: patientId,
        platform: "Android",
        topic: "WM Package",
        consultId:"0",
        contentId:"0",
        transactionDate : new Date()
        
    })
    })
    .then(response =>response.json()).then(responseData=>{
        console.log(`ResponseData in ${eventType}`,responseData)
    })
}

export default captureEvents